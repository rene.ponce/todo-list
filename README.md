# Aplicación Todo List

Aplicación para examen técnico.


# Descripción

Aplicación desarrollada con NodeJS para la construcción de servicios, MongoDB para almacenar la información así como el ODM mongoose, la aplicación móvil está desarrollada en Ionic utilizando el framework Angular en su versión 10

# Instrucciones

Entrar al folder **api** y ejecutar el comando **npm install** para que todas las dependencias sean instaladas. Una vez instaladas las dependencias ejecutar **npm start** para levantar el servicio.

Entrar al folder **appMovil** y ejecutar el comando **npm install** una vez instaladas las dependencias ejecutar **npm  install @capacitor/core @capacitor/cli** para instalar capacitor.

Para agregar una plataforma seguir los siguientes pasos:

 - ionic build
 - npx cap add android o npx cap add ios
 - npx cap open android o npx cap open ios
