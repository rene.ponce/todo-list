export interface ITask {
  _id?: string;
  name: string;
  done?: boolean;
  delete?: boolean;
  __v?: number;
}
