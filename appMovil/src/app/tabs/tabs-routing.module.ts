import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [
      {
        path: 'pending-tasks',
        loadChildren: () => import('../pages/pending-tasks/pending-tasks.module').then( m => m.PendingTasksPageModule)
      },
      {
        path: 'done-tasks',
        loadChildren: () => import('../pages/done-tasks/done-tasks.module').then( m => m.DoneTasksPageModule)
      },
      {
        path: 'create-task-modal',
        loadChildren: () => import('../pages/create-task-modal/create-task-modal.module').then( m => m.CreateTaskModalPageModule)
      },
      {
        path: '',
        redirectTo: '/tabs/pending-tasks',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/tabs/pending-tasks',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TabsPageRoutingModule {}
