import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TasksService {

  constructor(private http: HttpClient) { }

  getTasks(): Promise<any> {
    return this.http.get(`${environment.api}todos`).toPromise();
  }

  getDoneTasks(): Promise<any> {
    return this.http.get(`${environment.api}todos/done`).toPromise();
  }

  getPendingTasks(): Promise<any> {
    return this.http.get(`${environment.api}todos/nodone`).toPromise();
  }

  createTask(data: any): Promise<any> {
    return this.http.post(`${environment.api}todos`, data).toPromise();
  }

  updateTask(id: string, data: any): Promise<any> {
    return this.http.put(`${environment.api}todos/${id}`, data).toPromise();
  }

  deleteTask(id: string): Promise<any> {
    return this.http.delete(`${environment.api}todos/${id}`).toPromise();
  }
}
