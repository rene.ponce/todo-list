import { Component, OnInit } from '@angular/core';
import { ITask } from 'src/app/interfaces/itask';
import { TasksService } from 'src/app/services/tasks.service';

@Component({
  selector: 'app-done-tasks',
  templateUrl: './done-tasks.page.html',
  styleUrls: ['./done-tasks.page.scss'],
})
export class DoneTasksPage implements OnInit {

  doneTaskList = [];

  constructor(private tasksService: TasksService) { }

  ngOnInit() {

  }

  ionViewWillEnter() {
    this.getDoneTasksList();
  }

  getDoneTasksList() {
    this.tasksService.getDoneTasks().then(response => {
      this.doneTaskList = response;
    }).catch(error => {});
  }

  deleteTask(data: ITask) {
    this.tasksService.deleteTask(data._id).then(() => {
      this.getDoneTasksList();
    }).catch(error => {});
  }

}
