import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ITask } from 'src/app/interfaces/itask';
import { TasksService } from 'src/app/services/tasks.service';
import { CreateTaskModalPage } from '../create-task-modal/create-task-modal.page';

@Component({
  selector: 'app-pending-tasks',
  templateUrl: './pending-tasks.page.html',
  styleUrls: ['./pending-tasks.page.scss'],
})
export class PendingTasksPage implements OnInit {

  pendingTaskList = [];

  constructor(private tasksService: TasksService,
              private modalCtrl: ModalController) { }

  ngOnInit() {

  }

  ionViewWillEnter() {
    this.getPendingTasksList();
  }

  getPendingTasksList() {
    this.tasksService.getPendingTasks().then(response => {
      console.log(response);
      this.pendingTaskList = response;
    }).catch(error => {});
  }

  doneTask(data: ITask) {
    const task: ITask = {
      name: data.name,
      done: !data.done
    };
    this.tasksService.updateTask(data._id, task).then(response => {
      this.getPendingTasksList();
    }).catch(error => {});
  }

  deleteTask(data: ITask) {
    this.tasksService.deleteTask(data._id).then(() => {
      this.getPendingTasksList();
    }).catch(error => {});
  }

  async presentCreateTaskModal() {
    this.modalCtrl.create({
      component: CreateTaskModalPage,
      presentingElement: await this.modalCtrl.getTop()
    }).then(modal => {
      modal.onWillDismiss().then(() => {
        this.getPendingTasksList();
      });
      modal.present();
    });
  }

}
