import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CreateTaskModalPage } from './create-task-modal.page';

const routes: Routes = [
  {
    path: '',
    component: CreateTaskModalPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CreateTaskModalPageRoutingModule {}
