import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CreateTaskModalPageRoutingModule } from './create-task-modal-routing.module';

import { CreateTaskModalPage } from './create-task-modal.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CreateTaskModalPageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [CreateTaskModalPage]
})
export class CreateTaskModalPageModule {}
