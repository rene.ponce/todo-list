import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ModalController } from '@ionic/angular';
import { TasksService } from 'src/app/services/tasks.service';

@Component({
  selector: 'app-create-task-modal',
  templateUrl: './create-task-modal.page.html',
  styleUrls: ['./create-task-modal.page.scss']
})
export class CreateTaskModalPage implements OnInit {

  taskForm: FormGroup;
  isSubmitted = false;

  constructor(private fb: FormBuilder,
              private modalCtrl: ModalController,
              private tasksService: TasksService) { }

  ngOnInit() {
    this.taskForm = this.fb.group({
      name: ['', [Validators.required]]
    });
  }

  get name() {
    return this.taskForm.get('name');
  }

  get errorControl() {
    return this.taskForm.controls;
  }

  onSubmit() {
    this.isSubmitted = true;
    if (this.taskForm.valid) {
      this.tasksService.createTask(this.taskForm.value).then(response => {
        console.log(response);
        this.closeModal();
      }).catch(error => {});
    }
  }

  closeModal() {
    this.modalCtrl.dismiss();
  }

}
