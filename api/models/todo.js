const mongoose = require('mongoose');
const { Schema } = mongoose;

const todoSchema = new Schema({
    name: {
        type: String
    },
    done: {
        type: Boolean,
        default: false
    },
    delete: {
        type: Boolean,
        default: false
    }
});

const todo = mongoose.model('todo', todoSchema);

module.exports = todo;