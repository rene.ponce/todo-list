const todo = require('../models/todo');

class TodoRepository {

    constructor(model) {
        this.model = model;
    }

    findAll() {
        return this.model.find();
    }

    findById(id) {
        return this.model.findById(id);
    }

    create(name) {
        const newTodo = { name };
        const item = new this.model(newTodo);
        return item.save();
    }

    updateById(id, object) {
        const query = {_id: id};
        return this.model.findOneAndUpdate(query, {$set: { name: object.name, done: object.done }});
    }

    deleteById(id) {
        const query = {_id: id};
        return this.model.findOneAndUpdate(query, {$set: {delete: true}});
    }

    findAllDone() {
        return this.model.find({$and:[{done: true}, {delete: false}]});
    }

    findAllNoDone() {
        const query = {done: false, delete: false};
        return this.model.find({$and:[{done: false, delete: false}]});
    }
}

module.exports = new TodoRepository(todo);