const createError   = require('http-errors'),
      express       = require('express'),
      mongoose      = require('mongoose'),
      cookieParser  = require('cookie-parser'),
      cors          = require('cors'),
      config        = require('./config/config'),
      routes        = require('./routes/routes'),
      app           = express();

mongoose.connect(config.DB, {
  useNewUrlParser: true,
  useUnifiedTopology: true
});

app.use(cors());

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

app.use('/todos', routes);

app.use((req, res, next) => {
  next(createError(404));
});

app.use((err, req, res) => {
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};
  res.status(err.status || 500);
  res.render('error');
});

app.listen(config.APP_PORT);

module.exports = app;