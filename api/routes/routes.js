const express = require('express');
const app = express.Router();
const repository = require('../repositories/todoRepository');

app.get('/', (req, res) => {
    repository.findAll().then((todos) => {
        res.json(todos);
    }).catch((error) => console.log(error));
});

app.get('/done', (req, res) => {
    repository.findAllDone().then((todo) => {
        res.json(todo);
    }).catch((error) => console.log(error));
});

app.get('/nodone', (req, res) => {
    repository.findAllNoDone().then((todo) => {
        res.json(todo);
    }).catch((error) => console.log(error));
});

app.post('/', (req, res) => {
    const { name } = req.body;
    repository.create(name).then((todo) => {
        res.json(todo);
    }).catch((error) => console.log(error));
});

app.put('/:id', (req, res) => {
    const { id } = req.params;
    const todo = { name: req.body.name, done: req.body.done };
    repository.updateById(id, todo)
        .then(res.status(200).json([]))
        .catch((error) => console.log(error));
});

app.delete('/:id', (req, res) => {
    const { id } = req.params;
    repository.deleteById(id)
        .then(res.status(200).json([]))
        .catch((error) => console.log(error));
});

module.exports = app;